app.controller('login-ctrl', function($scope, $http, $location, $cookies, Page){
    Page.setTitle('Login');
	if($cookies.get('token') != undefined)
		$location.url("/admin/dashboard");
	loginDesign();
	$scope.user = {
		email : '',
		pwd : ''
	};
	$scope.error = {
		email: '',
		pwd: '',
		gnrl:''
	};

	$scope.signIn = function(){
		$scope.error.email = '';
		$scope.error.pwd = '';
		$scope.error.gnrl = '';
		$http({
			method : 'POST',
			url : baseUrl + '/user/admin/login',
			data : $scope.user,
			headers : {
				'Content-Type': 'application/json'
			}
		}).then(function successCallback(response) {
			if(response.data.status == 200){
				if(angular.isArray(response.data.msg)){
					response.data.msg.forEach(function(element){
						$scope.error[element.key] = element.value;
						console.log($scope.error);
					});
				}else{
					$scope.error[response.data.msg.key] = response.data.msg.value;
				}
			}else{
		      $cookies.put("token", response.data.data.token);
          $location.url("/admin/dashboard");
			}
		}, function errorCallback(response) {
			console.log(response);
		});
	};
});

function loginDesign (){
	$('.login-item input').each(function(){
		$(this).focusin(function(){
			if($(this).val() == ''){
				var label = $("label[for='" + $(this).attr('id') + "']");
				label.css({
					'animation': 'inputFocusIn .5s linear forwards'
				});
				$(this).siblings(".line-active").css({
					'animation': 'lineFocusIn .2s linear forwards'
				});
			}
		}).focusout(function(){
			if($(this).val() == ''){
				var label = $("label[for='" + $(this).attr('id') + "']");
				label.css({
					'animation': 'inputFocusOut .5s linear forwards'
				});
				$(this).siblings(".line-active").css({
					'animation': 'lineFocusOut .2s linear forwards'
				});
			}
		});
	});
}
