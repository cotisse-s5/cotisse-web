app.controller('driver-ctrl', function($scope, $http, $location, $cookies, Page){
	Page.setTitle('Driver');
    checkCookie($cookies, $location);
	$scope.driver = {
		frstName : '',
		lastName : '',
		email : '',
		num : ''
	};
	$scope.error = [];
	$scope.drivers = [];
	listDriver($scope, $http, $cookies);
	insertDriver($scope, $http, $cookies);
});

function insertDriver($scope, $http, $cookies){
	$scope.insert = function(){
		console.log($scope.driver);
		$http({
			method : 'POST',
			url : baseUrl + '/admin/user/insertDriver',
			data : $scope.driver,
			headers : {
				'Content-Type': 'application/json',
				'Authorization': 'bearer ' + $cookies.get('token'),
			}
		}).then(function successCallback(response) {
			if(response.data.status == 401){
				if(angular.isArray(response.data.msg)){
					response.data.msg.forEach(function(element){
						$scope.error[element.key] = element.value;
					});
				}else{
					console.log(response.data.msg);
				}
			}else{
				$scope.drivers = [];
				listDriver($scope, $http, $cookies);
			}
		}, function errorCallback(response) {
			console.log(response);
		});
	}
}

function listDriver($scope, $http, $cookies){
	$http({
		method : 'GET',
		url : baseUrl + '/user/driver/list',
		headers : {
			'Content-Type': 'application/json',
			'Authorization': 'bearer ' + $cookies.get('token'),
		}
	}).then(function successCallback(response) {
		if(response.data.status == 401){
			if(angular.isArray(response.data.msg)){
				response.data.msg.forEach(function(element){
					$scope.error[element.key] = element.value;
					console.log($scope.error);
				});
			}else{
				console.log(response.data.msg);
			}
		}else{
			response.data.data.forEach(function(element){
				$scope.drivers.push(element);
			});
		}
	}, function errorCallback(response) {
		console.log(response);
	});
}
