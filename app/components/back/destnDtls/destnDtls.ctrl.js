app.controller('destnDtls-ctrl', function($scope, $http, $location, $cookies, Page){
    Page.setTitle('Destination Details');
    checkCookie($cookies, $location);
    listCity($scope, $http, $cookies);
    $scope.destnDetsSearch = {
      idDeparture: "",
      idArrival: "",
      departureDate: "",
    }

    $scope.searchDestnDets = function(){
      console.log($scope.destnDetsSearch);
      $http({
        method : 'POST',
        url : baseUrl + '/destnDets/listDts',
        data : $scope.destnDetsSearch,
        headers : {
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + $cookies.get('token'),
        }
      }).then(function successCallback(response) {
        if(response.data.status == 401){
          $scope.error = {
            departure : response.data.msg['departure'],
            arrival : response.data.msg['arrival'],
            departureDate : response.data.msg['departureDate'],
          }
        }else{
          $scope.listDestnDets = response.data.data;
          for(var i = 0; i < $scope.listDestnDets.length; i++){
            var d = new Date($scope.listDestnDets[i].departureDate);
            var hour = d.getHours() > 9 ? "" + d.getHours() : "0" + d.getHours();
            var min = d.getMinutes() > 9 ? "" + d.getMinutes() : "0" + d.getMinutes();
            $scope.listDestnDets[i].time = hour + ":" + min;
          }
          console.log($scope.listDestnDets);
        }
      }, function errorCallback(response) {
        console.log(response);
      });
    }


    /* EDIT */
    $scope.destnDets = {
      idDestn:"",
    	idVehicle:"",
    	departureDate:"",
    	arrivalDate:"",
    	destnPrice:"",
    }
    $scope.category = "";
    $scope.destns = [];
    $scope.categs = [];
    $scope.vehicles = [];
    listDestn($scope, $http, $cookies);
    $scope.getCateg = function(){
      $scope.destns.forEach((destn) => {
        if(destn.idDestn == $scope.destnDets.idDestn)
          $scope.categs = destn.categs;
      });
    };
    $scope.getVehAndPrice = function(){
      $scope.categs.forEach((categ) => {
        if(categ.idCateg == $scope.category){
          $scope.destnDets.destnPrice = categ.price;
          listVehCateg($scope, $http, $cookies);
        }
      });
    };
    insertDestnDets($scope, $http, $cookies);
});

function insertDestnDets($scope, $http, $cookies){
  $scope.error = {};
  if($scope.destnDets.destnPrice == "")
    $scope.destnDets.destnPrice = 0;
  $scope.insert = function(){
    $http({
      method : 'POST',
      url : baseUrl + '/admin/destnDets/insert',
      data : $scope.destnDets,
      headers : {
        'Content-Type': 'application/json',
        'Authorization': 'bearer ' + $cookies.get('token'),
      }
    }).then(function successCallback(response) {
      if(response.data.status == 401){
        $scope.error = {
          destination : response.data.msg['destination'],
          departure : response.data.msg['departure'],
          arrival : response.data.msg['arrival'],
          price : response.data.msg['price'],
          vehicle : response.data.msg['vehicle'],
        }
      }else{
        alert("Destination inserted successfully");
      }
    }, function errorCallback(response) {
      console.log(response);
    });
  }
}

function listVehCateg($scope, $http, $cookies){
  $http({
    method : 'GET',
    url : baseUrl + '/admin/vehicle/list/' + $scope.category,
    headers : {
      'Content-Type': 'application/json',
      'Authorization': 'bearer ' + $cookies.get('token'),
    }
  }).then(function successCallback(response) {
    if(response.data.status == 401){
      if(angular.isArray(response.data.msg)){
        response.data.msg.forEach(function(element){
          $scope.error[element.key] = element.value;
          console.log($scope.error);
        });
      }else{
        console.log(response.data.msg);
      }
    }else{
      $scope.vehicles = response.data.data;
    }
  }, function errorCallback(response) {
    console.log(response);
  });
}
