app.controller('destn-ctrl', function($scope, $http, $location, $cookies, Page){
    Page.setTitle('destn');
    checkCookie($cookies, $location);
    $scope.destns = [];
    $scope.categs = [];
    $scope.cities = [];
    $scope.destn = {
    		idDeparture : "test",
    		idArrival : "test",
    		idCateg : "test",
    		price : ""
    };
    listCity($scope, $http, $cookies);
    listCateg($scope, $http, $cookies);
    listDestn($scope, $http, $cookies);
    insertDestn($scope, $http, $cookies);

    console.log($scope.destns);
});

function insertDestn($scope, $http, $cookies){
	$scope.insert = function(){
		$http({
			method : 'POST',
			url : baseUrl + '/admin/destn/insert',
			data : $scope.destn,
			headers : {
				'Content-Type': 'application/json',
				'Authorization': 'bearer ' + $cookies.get('token')
			}
		}).then(function successCallback(response) {
			if(response.data.status == 401){
				if(angular.isArray(response.data.msg)){
					response.data.msg.forEach(function(element){
						$scope.error[element.key] = element.value;
						console.log($scope.error);
					});
				}else{
					console.log(response.data.msg);
				}
			}else{
				$scope.destns = [];
				listDestn($scope, $http, $cookies);
			}
		}, function errorCallback(response) {
			console.log(response);
		});
	};
}

function listDestn($scope, $http, $cookies){
	$http({
		method : 'GET',
		url : baseUrl + '/admin/destination/list',
		data : $scope.user,
		headers : {
      'Content-Type': 'application/json',
      'Authorization': 'bearer ' + $cookies.get('token')
    }
	}).then(function successCallback(response) {
		if(response.data.status == 401){
			if(angular.isArray(response.data.msg)){
				response.data.msg.forEach(function(element){
					$scope.error[element.key] = element.value;
					console.log($scope.error);
				});
			}else{
				console.log(response.data.msg);
			}
		}else{
			response.data.data.forEach(function(element){
				$scope.destns.push(element);
			});
		}
	}, function errorCallback(response) {
		console.log(response);
	});
}

function listCateg($scope, $http, $cookies){
	$http({
		method : 'GET',
		url : baseUrl + '/admin/categ/list',
		data : $scope.user,
		headers : {
      'Content-Type': 'application/json',
      'Authorization': 'bearer ' + $cookies.get('token'),
    }
	}).then(function successCallback(response) {
		if(response.data.status == 401){
			if(angular.isArray(response.data.msg)){
				response.data.msg.forEach(function(element){
					$scope.error[element.key] = element.value;
					console.log($scope.error);
				});
			}else{
				console.log(response.data.msg);
			}
		}else{
			response.data.data.forEach(function(element){
				$scope.categs.push({
					idCateg: element.idCateg,
					label: element.label
				});
			});
		}
	}, function errorCallback(response) {
		console.log(response);
	});
}

function listCity($scope, $http, $cookies){
	$http({
		method : 'GET',
		url : baseUrl + '/admin/city/list',
		data : $scope.user,
		headers : {
      'Content-Type': 'application/json',
      'Authorization': 'bearer ' + $cookies.get('token'),
    }
	}).then(function successCallback(response) {
		if(response.data.status == 401){
			if(angular.isArray(response.data.msg)){
				response.data.msg.forEach(function(element){
					$scope.error[element.key] = element.value;
					console.log($scope.error);
				});
			}else{
				console.log(response.data.msg);
			}
		}else{
      $scope.cities = response.data.data;
		}
	}, function errorCallback(response) {
		console.log(response);
	});
}
