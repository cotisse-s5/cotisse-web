app.controller('vehicle-ctrl', function($scope, $http, $location, $cookies, Page){
    Page.setTitle('List vehicle');
    checkCookie($cookies, $location);
    $scope.categs = [];
    $scope.drivers = [];
    $scope.brands = [];
    $scope.listChairs = [];
    $scope.vehicles = [];
    listCategVeh($scope, $http, $cookies);
    driverAvailable($scope, $http, $cookies);
    listBrand($scope, $http, $cookies);
    listVeh($scope, $http, $cookies);
    $scope.vehForm = {
      idUsers: '',
      idBrand: '',
      regNb:'',
      idCateg: 'none',
      chairs: '',
    }
    insertVehicle($scope, $http, $cookies);
    $scope.setPlace = function(idCateg){
      $scope.categs.forEach((categ) =>{
        if(categ.idCateg == idCateg)
          setPlace($scope, categ.nbColumn, categ.nbRow);
      });
    }
    $scope.changeState = function (id){
      var idChair = -1;
      var index = -1;
      if($scope.vehForm.idCateg != 'none'){
        for(var i = 0; i < $scope.listChairs.length; i++){
          for(var j = 0; j < $scope.listChairs[i].length; j++){
            if($scope.listChairs[i][j].id != id){
              if($scope.listChairs[i][j].id > idChair && idChair > 0)
                $scope.listChairs[i][j].num = $scope.listChairs[i][j].num - index;
              continue;
            }
            if($scope.listChairs[i][j].num > 0){
              index = 1;
              idChair = $scope.listChairs[i][j].id;
              $(".btn-chair[value=" + id + "]").addClass('chair-inactive');
              $scope.listChairs[i][j].num = -1;
              $scope.listChairs[i][j].state = -1;
            }else{
              index = -1;
              var tmp = 0;
              for(var k = 0; k < $scope.listChairs.length; k++){
                for(var t = 0; t < $scope.listChairs[k].length; t++){
                  if($scope.listChairs[k][t].id >= $scope.listChairs[i][j].id)
                    break;
                  if($scope.listChairs[k][t].num < 0)
                    continue;
                  tmp = $scope.listChairs[k][t].num;
                }
              }
              idChair = $scope.listChairs[i][j].id;
              $scope.listChairs[i][j].num = tmp +1;
              $scope.listChairs[i][j].state = 1;
              $(".btn-chair[value=" + id + "]").removeClass('chair-inactive');
            }
          }
        }
      }
    }
});

function insertVehicle($scope, $http, $cookies){
  $scope.insert = function(){
    var listChairs = [];
    $scope.listChairs.forEach((chairs)=>{
      chairs.forEach((chair)=>{
        listChairs.push(chair);
      });
    });
    $scope.vehForm.chairs = listChairs;
    $http({
      method : 'POST',
      url : baseUrl + '/admin/vehicle/insert',
      data : $scope.vehForm,
      headers : {
        'Content-Type': 'application/json',
        'Authorization': 'bearer ' + $cookies.get('token'),
      }
    }).then(function successCallback(response) {
      if(response.data.status == 401){
        if(angular.isArray(response.data.msg)){
          response.data.msg.forEach(function(element){
            $scope.error[element.key] = element.value;
          });
        }else{
          console.log(response.data.msg);
        }
      }else{
        driverAvailable($scope, $http, $cookies);
      }
    }, function errorCallback(response) {
      console.log(response);
    });
  }
}

function driverAvailable($scope, $http, $cookies){
	$http({
		method : 'GET',
		url : baseUrl + '/user/driver/available',
		headers : {
			'Content-Type': 'application/json',
			'Authorization': 'bearer ' + $cookies.get('token'),
		}
	}).then(function successCallback(response) {
		if(response.data.status == 401){
			if(angular.isArray(response.data.msg)){
				response.data.msg.forEach(function(element){
					$scope.error[element.key] = element.value;
					console.log($scope.error);
				});
			}else{
				console.log(response.data.msg);
			}
		}else{
      $scope.drivers = response.data.data;
		}
	}, function errorCallback(response) {
		console.log(response);
	});
}

function listVeh($scope, $http, $cookies){
	$http({
		method : 'GET',
		url : baseUrl + '/admin/vehicle/list',
		data : $scope.user,
		headers : {
      'Content-Type': 'application/json',
      'Authorization': 'bearer ' + $cookies.get('token'),
    }
	}).then(function successCallback(response) {
		if(response.data.status == 401){
			if(angular.isArray(response.data.msg)){
				response.data.msg.forEach(function(element){
					$scope.error[element.key] = element.value;
					console.log($scope.error);
				});
			}else{
				console.log(response.data.msg);
			}
		}else{
      $scope.vehicles = response.data.data;
		}
	}, function errorCallback(response) {
		console.log(response);
	});
}
function listCategVeh($scope, $http, $cookies){
	$http({
		method : 'GET',
		url : baseUrl + '/admin/categ/list',
		data : $scope.user,
		headers : {
      'Content-Type': 'application/json',
      'Authorization': 'bearer ' + $cookies.get('token'),
    }
	}).then(function successCallback(response) {
		if(response.data.status == 401){
			if(angular.isArray(response.data.msg)){
				response.data.msg.forEach(function(element){
					$scope.error[element.key] = element.value;
					console.log($scope.error);
				});
			}else{
				console.log(response.data.msg);
			}
		}else{
			response.data.data.forEach(function(element){
				$scope.categs.push(element);
			});
		}
	}, function errorCallback(response) {
		console.log(response);
	}).then(function(){
    if($scope.categs.length != 0)
      setPlace($scope, $scope.categs[0].nbColumn, $scope.categs[0].nbRow);
  });
}
function listBrand($scope, $http, $cookies){
  $http({
    method : 'GET',
    url : baseUrl + '/admin/brand/list',
    headers : {
      'Content-Type': 'application/json',
      'Authorization': 'bearer ' + $cookies.get('token'),
    }
  }).then(function successCallback(response) {
    if(response.data.status == 401){
      if(angular.isArray(response.data.msg)){
        response.data.msg.forEach(function(element){
          $scope.error[element.key] = element.value;
          console.log($scope.error);
        });
      }else{
        console.log(response.data.msg);
      }
    }else{
      response.data.data.forEach(function(element){
        $scope.brands.push(element);
      });
    }
  }, function errorCallback(response) {
    console.log(response);
  });
}
function setPlace($scope, column, row){
  $scope.listChairs = [];
  var num = 0, id = 0;
  for(var i = 0; i < row; i++){
    var chairs = [];
    for(var j = 0; j < column; j++){
      chairs.push({
        id : ++id,
        num: ++num,
        state: 1
      });
    }
    $scope.listChairs.push(chairs);
  }
}
