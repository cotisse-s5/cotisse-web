app.controller('category-ctrl', function($scope, $http, $location, $cookies, Page){
  Page.setTitle('Category');
  checkCookie($cookies, $location);
  $scope.categs = [];
  listCateg($scope, $http, $cookies);
  console.log($scope.categs);
});

function listCateg($scope, $http, $cookies){
	$http({
		method : 'GET',
		url : baseUrl + '/admin/categ/list',
		headers : {
      'Content-Type': 'application/json',
      'Authorization': 'bearer ' + $cookies.get('token'),
    }
	}).then(function successCallback(response) {
		if(response.data.status == 401){
			if(angular.isArray(response.data.msg)){
				response.data.msg.forEach(function(element){
					$scope.error[element.key] = element.value;
					console.log($scope.error);
				});
			}else{
				console.log(response.data.msg);
			}
		}else{
			response.data.data.forEach(function(element){
				$scope.categs.push(element);
			});
		}
	}, function errorCallback(response) {
		console.log(response);
	});
}
