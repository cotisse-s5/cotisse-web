//var baseUrl ="https://cotisse-jk.herokuapp.com";
var baseUrl = 'http://localhost:5000';
var app = angular.module('app', ['ngRoute', 'ngCookies']);

function checkCookie($cookies, $location){
	if($cookies.get('token') == undefined)
		$location.url("/admin/login");
}
