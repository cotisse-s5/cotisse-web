app.config(function($routeProvider){
    $routeProvider
    .when('/',{
        templateUrl: './app/components/front/home/home.html',
        controller: 'home-ctrl',
    }).when('/admin/login',{
        templateUrl: './app/components/back/login/login.html',
        controller: 'login-ctrl',
    }).when('/admin/dashboard',{
        templateUrl:'./app/components/back/dashboard/dashboard.html',
        controller: 'dashboard-ctrl',
    }).when('/admin/destn/list',{
        templateUrl:'./app/components/back/destn/destn.html',
        controller: 'destn-ctrl',
    }).when('/admin/destn/edit',{
        templateUrl:'./app/components/back/destn/destnEdit.html',
        controller: 'destn-ctrl',
    }).when('/admin/vehicle/list',{
        templateUrl:'./app/components/back/vehicle/vehicle.html',
        controller: 'vehicle-ctrl',
    }).when('/admin/vehicle/edit',{
        templateUrl:'./app/components/back/vehicle/vehicleEdit.html',
        controller: 'vehicle-ctrl',
    }).when('/admin/destnDets/list',{
        templateUrl:'./app/components/back/destnDtls/destnDtls.html',
        controller: 'destnDtls-ctrl',
    }).when('/admin/destnDets/edit',{
        templateUrl:'./app/components/back/destnDtls/destnDtlsEdit.html',
        controller: 'destnDtls-ctrl',
    }).when('/admin/driver',{
        templateUrl:'./app/components/back/driver/driver.html',
        controller: 'driver-ctrl',
    }).when('/admin/categories',{
        templateUrl:'./app/components/back/category/category.html',
        controller: 'category-ctrl',
    });
});
