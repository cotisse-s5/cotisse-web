app.controller('sidenav-ctrl', function($scope, $http, $location, $cookies, Page){

	$scope.deconnection = function(){
		if($cookies.get('token') != undefined){
			$cookies.remove('token');
			$location.url("/admin/login");
		}
	}

	$('.sidenav-dropdown').each(function(){
		$(this).on('click', function(){
			var submenu = $(this).children('.submenu');
			if(submenu.hasClass('submenu-show')){
				submenu.removeClass('submenu-show');
				$(this).children('.sidenav-subitem-title').children('.dropdown-icon').removeClass('sidenav-subitem-title-rotate');
			}else{
				$('.submenu').each(function(){
					if($(this).hasClass('submenu-show')){
						$(this).removeClass('submenu-show');
						$(this).siblings('.sidenav-subitem-title').children('.dropdown-icon').removeClass('sidenav-subitem-title-rotate');
					}
				});
				submenu.addClass('submenu-show');
				$(this).children('.sidenav-subitem-title').children('.dropdown-icon').addClass('sidenav-subitem-title-rotate');
			}
		});
	});
});
